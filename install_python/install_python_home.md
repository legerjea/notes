# Environnement à avoir systèmatiquement

Sourcer le fichier `uselocal`, pour créer le `.local` et pour l'utiliser à
chaque fois.

```
… $ . uselocal
```

```
LD_LIBRARY_PATH=$HOME/.local/lib:$HOME/.local/lib64
LD_RUN_PATH=$HOME/.local/lib:$HOME/.local/lib64
LIBRARY_PATH=$HOME/.local/lib:$HOME/.local/lib64
CPATH=$HOME/.local/include
PKG_CONFIG_PATH=$HOME/.local/lib/pkgconfig
PATH=$HOME/.local/bin:$PATH

export LD_LIBRARY_PATH
export LD_RUN_PATH
export LIBRARY_PATH
export CPATH
export PKG_CONFIG_PATH
export PATH
```

# Toolchain

## binutils

```
… $ wget ftp://ftp.lip6.fr/pub/gnu/binutils/binutils-2.32.tar.xz
… $ tar xvf …
… $ cd …
…/binutils… $ ./configure --prefix=$HOME/.local
…/binutils… $ make -j 40
…/binutils… $ make install
```

## gcc

```
… $ wget ftp://ftp.lip6.fr/pub/gcc/releases/gcc-8.3.0/gcc-8.3.0.tar.xz
… $ tar xvf …
… $ cd …
…/gcc… $ ./contrib/download_prerequisites
…/gcc… $ mkdir ../objdir_gcc
…/gcc… $ cd ../objdir_gcc
…/objdir_gcc $ ../gcc-…/configure --prefix=$HOME/.local --enable-languages=c,c++,fortran,go --disable-multilib
…/objdir_gcc $ make -j 40
…/objdir_gcc $ make install
```

Vérif:

```
… $ hash -r
… $ gcc -v
```

Clean (parce que c'est gros)

```
… $ rm -rf objdir_gcc
```

# Usefull libs

## libffi

```
… $ wget ftp://sourceware.org/pub/libffi/libffi-3.2.1.tar.gz
… $ tar xvf …
… $ cd …
…/ffi… $ ./configure --prefix=$HOME/.local
…/ffi… $ make -j 40
…/ffi… $ make install
```

## Openblas

```
… $ wget http://github.com/xianyi/OpenBLAS/archive/v0.2.20.tar.gz
… $ tar xvf v0…
… $ cd OpenBLAS…
…/OpenBLAS… $ make -j 40
…/OpenBLAS… $ make PREFIX=$HOME/.local install
```

## TCL/TK

Utile seulement si `tclsh` est absent

```
… $ wget https://prdownloads.sourceforge.net/tcl/tcl8.6.9-src.tar.gz
… $ tar xvf tcl…
… $ cd tcl…/unix/
…/tcl…/unix $ ./configure --prefix=$HOME/.local
…/tcl…/unix $ make -j 40
…/tcl…/unix $ make install
```

```
… $ wget https://prdownloads.sourceforge.net/tcl/tk8.6.9.1-src.tar.gz
… $ tar xvf tk…
… $ cd tk…/unix/
…/tk…/unix $ ./configure --prefix=$HOME/.local
…/tk…/unix $ make -j 40
…/tk…/unix $ make install
```

## SQlite

```
… $ wget https://www.sqlite.org/src/tarball/sqlite.tar.gz?r=release -O sqlite.tar.gz
… $ tar xvf …
… $ cd sqlite
…/sqlite $ ./configure --prefix=$HOME/.local
…/sqlite $ make -j 40
…/sqlite $ make install
```


# Python

## Core

```
… $ wget https://www.python.org/ftp/python/3.7.3/Python-3.7.3.tar.xz
… $ tar xvf …
… $ cd …
…/Python… $ LDFLAGS=`pkg-config --libs-only-L libffi` ./configure --prefix=$HOME/.local --enable-optimizations
…/Python… $ make -j 40
…/Python… $ make install
… $ ln -s $HOME/.local/bin/python3 $HOME/.local/bin/python
… $ ln -s $HOME/.local/bin/pip3 $HOME/.local/bin/pip
```

## Python stuff

```
pip install --upgrade pip
pip install numpy scipy matplotlib ipython pythran pandas statsmodels flatlatex
```
